# -*- coding: utf-8 -*-
from openerp import api, fields, models, _

class StockMove(models.Model):
    _inherit = 'stock.move'
    
    # scheduler to send mail when procuremnt product running out of stock

    @api.model
    def automatic_send_mail_poutstck(self):
        out_of_stock=self.get_out_of_stock_products()
        if out_of_stock is not False:
            template_id = self.env.ref('mail_out_of_stock.new_email_template_stock')
            email_to=template_id.email_id.ids
            recipient_ids= [(4, pid) for pid in email_to]
            template_id.send_mail(template_id.id,force_send=True,email_values={'email_to':email_to,'recipient_ids':recipient_ids})

    @api.model
    def get_out_of_stock_products(self):
        move_obj=self.env['stock.move']
        move_ids=move_obj.search([('procurement_id','!=',False),('procurement_id.state','in',('running','confirmed'))])
        final_list=[]
        if move_ids:
            vals={}
            for each_move in move_ids:
                available_qty = each_move.product_id.with_context({'location' : each_move.location_dest_id.id}).qty_available
                pid=each_move.product_id
                location_id=each_move.location_dest_id
                qty_avbl=available_qty
                if qty_avbl<2:
                    vals={
                    'pid':pid.name,
                    'location_id':location_id.name,
                    'avbl_qty':qty_avbl
                    }
                    final_list.append(vals)
        return final_list if len(final_list)>0 else False
         