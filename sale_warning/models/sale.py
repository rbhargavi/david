# -*- coding: utf-8 -*-
from openerp import api, fields, models, _
from datetime import datetime

class SaleOrder(models.Model):
    _inherit='sale.order'

    def onchange_requested_date(self, cr, uid, ids, requested_date,
                                commitment_date, context=None):
        if requested_date:
            current_date=datetime.now().date()
            req_date = datetime.strptime(str(requested_date), '%Y-%m-%d %H:%M:%S')
            # to check that the requested date is not less than the current date
            if req_date.date()< current_date:
                warning = {
                    'title': (_('Warning!')),
                    'message': (_('Requested Date should not be less than current date!'))
                }
                return {'warning': warning}
        return super(SaleOrder, self).onchange_requested_date(cr, uid, ids, requested_date, commitment_date, context=context)