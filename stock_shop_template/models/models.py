# -*- coding: utf-8 -*-

from openerp import models, fields, api
from datetime import date, datetime, timedelta
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from lxml import etree
from openerp import SUPERUSER_ID

# class stock_shop_template(models.Model):
#     _name = 'stock_shop_template.stock_shop_template'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

TEMPLATE_FLAG = "[template]"

class StockWarehouse(models.Model) :
	# _name = 'stock.picking'
	_inherit = 'stock.warehouse'

	user_ids = fields.One2many('res.users', 'main_warehouse_id', string='Users')


class StockPicking(models.Model) :
    # _name = 'stock.picking'
    _inherit = 'stock.picking'
    
    #check login user main warehouse  user or not 
    @api.multi
    def check_login_user_main_wh_compute(self):
        main_wh = self.env.ref('stock.warehouse0', raise_if_not_found=False)
        login_user_wh=self.env.user.main_warehouse_id
        if login_user_wh!=main_wh and self.temp_create_pick==True:
            self.check_login_user_wh=True
        
    #check picking created from Template Cron
    temp_create_pick = fields.Boolean('Template Created Picking', readonly=1)
    check_login_user_wh = fields.Boolean('Main Wh User or Not',compute='check_login_user_main_wh_compute', store=False, help="If login user main warehouse  user boolean will be false")
    
    # To set main company on click of  Mark as Todo button
    def action_confirm(self, cr, uid, ids, context=None):
        company_changed = False
        for picking in self.browse(cr, SUPERUSER_ID, ids, context=context):
            mainwh=picking.env.ref('stock.warehouse0', raise_if_not_found=False)
            if picking.temp_create_pick==True and mainwh:
                if picking.company_id.id != mainwh.company_id.id :
                    company_changed = True
                    picking.company_id=mainwh.company_id.id

                for move_line in picking.move_lines:
                    move_line.company_id=mainwh.company_id.id
                    # to convert product_uom and quantity
                    move_line.product_uom_qty = picking.env['product.uom']._compute_qty(move_line.product_uom.id,
                                                                                     move_line.product_uom_qty,
                                                                                     move_line.product_id.uom_id.id)
                    move_line.product_uom = move_line.product_id.uom_id


        res=super(StockPicking, self).action_confirm(cr, company_changed and SUPERUSER_ID or uid, ids, context)
        return res
    # @api.model
    # def _cron_generate_provision(self) :
    #     day_today = date.today().weekday()
    #     # check and cancel pickings which are in draft state
    #     pending_picking_ids=self.search([('state','=','draft'),('temp_create_pick','=',True)])
    #     if pending_picking_ids:
    #         for pick_id in pending_picking_ids:
    #             pick_id.action_cancel()
    #     for picking in self.search([('origin', 'ilike', TEMPLATE_FLAG), ('state', "=", 'draft')]) :
    #             day_picking = datetime.strptime(picking.min_date, DEFAULT_SERVER_DATETIME_FORMAT).weekday()
    #             if day_today == day_picking :
    #                 new_picking = picking.copy()
    #                 new_picking.temp_create_pick = True
    #                 new_picking.operating_unit_id=None
    #                 # reset picking company by destination company if picking generate through cron
    #                 new_picking.company_id=picking.location_dest_id.company_id.id
    #                 new_picking.origin = picking.origin[len(TEMPLATE_FLAG):]
    #                 new_picking.min_date = date.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
    #                 for move_line in new_picking.move_lines:
    #                     move_line.temp_create_move=True