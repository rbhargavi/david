# -*- coding: utf-8 -*-
from openerp import http
from openerp.addons.website_sale.controllers.main import website_sale
from openerp.http import request


class website_sale(website_sale):

    # to exclude product variants from publishing on website
    def get_attribute_value_ids(self, product):
        attrs=super(website_sale, self).get_attribute_value_ids(product)
        cr,uid=request.cr,request.uid
        if attrs:
            n=0
            for attr_id in attrs:
                product_id=request.registry.get('product.product').browse(cr, uid,attr_id[0])
                if product_id.exclude_from_website==True:
                    attrs.pop(n)
                n+=1
        return attrs